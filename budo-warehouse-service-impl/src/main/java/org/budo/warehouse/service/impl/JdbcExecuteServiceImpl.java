package org.budo.warehouse.service.impl;

import java.sql.Connection;
import java.util.Arrays;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.budo.graph.annotation.SpringGraph;
import org.budo.support.javax.sql.util.JdbcUtil;
import org.budo.warehouse.service.api.IDataNodeService;
import org.budo.warehouse.service.api.JdbcExecuteService;
import org.budo.warehouse.service.api.ServiceDynamicBeanProvider;
import org.budo.warehouse.service.entity.DataNode;
import org.budo.warehouse.service.entity.Pipeline;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Slf4j
@Service("jdbcExecuteServiceImpl")
public class JdbcExecuteServiceImpl implements JdbcExecuteService {
    @Resource
    private ServiceDynamicBeanProvider serviceDynamicBeanProvider;

    @Resource
    private IDataNodeService dataNodeService;

    @SpringGraph
    @Override
    public Integer executeUpdate(Pipeline pipeline, String sql, Object[] parameters) {
        Connection connection = this.getConnection(pipeline);

        Integer updateCount;
        try {
            updateCount = JdbcUtil.executeUpdate(connection, sql, parameters);
        } catch (Throwable e) {
            throw new RuntimeException("#58 executeUpdate error, pipeline=" + pipeline, e);
        }

        if (null == updateCount || updateCount < 1) {
            log.warn("#56 sql=" + sql + ", parameters=" + Arrays.toString(parameters) + ", updateCount=" + updateCount);
        }

        return updateCount;
    }

    private Connection getConnection(Pipeline pipeline) {
        Integer targetDataNodeId = pipeline.getTargetDataNodeId();
        String targetSchema = pipeline.getTargetSchema();

        DataNode targetDataNode = dataNodeService.findByIdCached(targetDataNodeId);
        DataSource dataSource = serviceDynamicBeanProvider.dataSource(targetDataNode, targetSchema);

        try {
            return dataSource.getConnection();
        } catch (Throwable e) {
            if (("" + e).contains("com.alibaba.druid.pool.DataSourceDisableException")) { // 不知为啥 druid.DataSource 会 disable
                log.error("#63 DataSourceDisableException, dataSource=" + dataSource);
                return JdbcUtil.getConnection(targetDataNode.getUrl(), targetDataNode.getUsername(), targetDataNode.getPassword());
            }

            // 其他异常
            throw new RuntimeException(e);
        }
    }
}