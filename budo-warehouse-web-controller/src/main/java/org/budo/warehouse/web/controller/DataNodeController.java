package org.budo.warehouse.web.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.budo.druid.util.DruidUtil;
import org.budo.support.dao.page.Page;
import org.budo.support.javax.sql.util.JdbcUtil;
import org.budo.support.javax.sql.util.ResultSetUtil;
import org.budo.support.mvcs.Mvcs;
import org.budo.warehouse.service.api.IDataNodeService;
import org.budo.warehouse.service.entity.DataNode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author lmw
 */
@Controller
public class DataNodeController {
    @Resource
    private IDataNodeService dataNodeService;

    @ResponseBody
    @RequestMapping("mysql_nodes")
    public String mysql_nodes() {
        List<DataNode> dataNodes = dataNodeService.listMysqlNodes(Page.max());
        String html = "<table border=\"1\">";
        html += "<tr>";
        html += "<td>ID</td>";
        html += "<td>URL</td>";
        html += "<td>Description</td>";
        html += "<td>GRANTS</td>";
        html += "</tr>";

        for (DataNode dataNode : dataNodes) {
            html += "<tr>";
            html += "<td>" + dataNode.getId() + "</td>";
            html += "<td>" + dataNode.getUrl() + "</td>";
            html += "<td>" + dataNode.getDescription() + "</td>";
            html += "<td>" + this.showGrants(dataNode) + "</td>";
            html += "</tr>";
        }

        html += "</table>";

        Mvcs.setContentType("text/html;charset=utf-8");
        return html;
    }

    private String showGrants(DataNode dataNode) {
        try {
            String username = dataNode.getUsername();

            String password = dataNode.getPassword();
            password = null == password ? "" : password;
            password = DruidUtil.rsaDecrypt(password); // 密码解密

            Connection connection = JdbcUtil.getConnection(dataNode.getUrl(), username, password);
            PreparedStatement preparedStatement = JdbcUtil.prepareStatement(connection, "SHOW GRANTS FOR " + username);
            preparedStatement.setQueryTimeout(3);
            ResultSet resultSet = JdbcUtil.executeQuery(preparedStatement);

            List<Map<String, Object>> grantList = ResultSetUtil.toMapList(resultSet, false);

            JdbcUtil.close(resultSet);
            JdbcUtil.close(preparedStatement);
            JdbcUtil.close(connection);

            String grants = "";
            for (Map<String, Object> grant : grantList) {
                for (Entry<String, Object> entry : grant.entrySet()) {
                    String value = entry.getValue() + "";
                    int a = value.lastIndexOf(" TO ");
                    if (a > 0) {
                        value = value.substring(0, a);
                    }
                    grants += value + "<br/>";
                }
            }
            return grants + "";
        } catch (Exception e) {
            return e + "";
        }
    }
}