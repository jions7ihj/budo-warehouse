package org.budo.warehouse.web.impl;

import java.util.List;

import javax.annotation.Resource;

import org.budo.support.dao.page.Page;
import org.budo.support.dao.page.PageModel;
import org.budo.warehouse.service.api.IFieldMappingService;
import org.budo.warehouse.service.entity.FieldMapping;
import org.budo.warehouse.web.api.FieldMappingApi;
import org.springframework.stereotype.Component;

/**
 * @author limingwei
 */
@Component
public class FieldMappingApiImpl implements FieldMappingApi {
    @Resource
    private IFieldMappingService fieldMappingService;

    @Override
    public PageModel<FieldMapping> listByPipelineId(Integer pipelineId, Page page) {
        List<FieldMapping> list = fieldMappingService.listByPipelineId(pipelineId);
        return new PageModel<FieldMapping>(list, page);
    }

    @Override
    public List<Integer> listCountByPipelineIds(List<Integer> pipelineIds) {
        return fieldMappingService.listCountByPipelineIds(pipelineIds);
    }
}