package org.budo.warehouse.web.impl;

import org.budo.warehouse.web.api.EntryBufferApi;
import org.springframework.stereotype.Component;

/**
 * @author limingwei
 */
@Component
public class EntryBufferApiImpl implements EntryBufferApi { }