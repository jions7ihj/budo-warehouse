package org.budo.warehouse.web.impl;

import javax.annotation.Resource;

import org.budo.support.dao.page.Page;
import org.budo.support.dao.page.PageModel;
import org.budo.warehouse.service.api.IPipelineService;
import org.budo.warehouse.service.entity.Pipeline;
import org.budo.warehouse.web.api.PipelineApi;
import org.springframework.stereotype.Component;

/**
 * @author limingwei
 */
@Component
public class PipelineApiImpl implements PipelineApi {
    @Resource
    private IPipelineService pipelineService;

    @Override
    public PageModel<Pipeline> list(Page page) {
        return pipelineService.list(page);
    }
}