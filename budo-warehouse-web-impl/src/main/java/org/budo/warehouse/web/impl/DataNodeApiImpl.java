package org.budo.warehouse.web.impl;

import java.util.List;

import javax.annotation.Resource;

import org.budo.support.dao.page.Page;
import org.budo.support.dao.page.PageModel;
import org.budo.warehouse.service.api.IDataNodeService;
import org.budo.warehouse.service.entity.DataNode;
import org.budo.warehouse.web.api.DataNodeApi;
import org.springframework.stereotype.Component;

/**
 * @author limingwei
 */
@Component
public class DataNodeApiImpl implements DataNodeApi {
    @Resource
    private IDataNodeService dataNodeService;

    @Override
    public PageModel<DataNode> list(Page page) {
        return dataNodeService.list(page);
    }

    @Override
    public List<String> listNameByIds(List<Integer> ids) {
        return dataNodeService.listNameByIds(ids);
    }
}