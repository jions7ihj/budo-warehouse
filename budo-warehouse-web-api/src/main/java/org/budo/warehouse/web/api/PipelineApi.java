package org.budo.warehouse.web.api;

import org.budo.dubbo.protocol.http.authentication.AuthenticationCheck;
import org.budo.support.dao.page.Page;
import org.budo.support.dao.page.PageModel;
import org.budo.warehouse.service.entity.Pipeline;

/**
 * @author limingwei
 */
@AuthenticationCheck
public interface PipelineApi {
    PageModel<Pipeline> list(Page page);
}