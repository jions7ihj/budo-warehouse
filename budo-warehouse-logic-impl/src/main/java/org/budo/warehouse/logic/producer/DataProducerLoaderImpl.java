package org.budo.warehouse.logic.producer;

import java.util.List;

import javax.annotation.Resource;

import org.budo.canal.server.BudoCanalServer;
import org.budo.support.dao.page.Page;
import org.budo.support.spring.context.aware.RootApplicationContextRefreshedEventListener;
import org.budo.warehouse.logic.api.DataProducerLoader;
import org.budo.warehouse.logic.bean.LogicDynamicBeanProvider;
import org.budo.warehouse.service.api.IDataNodeService;
import org.budo.warehouse.service.api.IPipelineService;
import org.budo.warehouse.service.entity.DataNode;
import org.budo.warehouse.service.entity.Pipeline;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author limingwei
 */
@Slf4j
@Component
@Order(Ordered.LOWEST_PRECEDENCE) // 在BudoAutoDdlSupport后
public class DataProducerLoaderImpl extends RootApplicationContextRefreshedEventListener implements DataProducerLoader {
    @Resource
    private IDataNodeService dataNodeService;

    @Resource
    private IPipelineService pipelineService;

    @Resource
    private BudoCanalServer budoCanalServer;

    @Resource
    private LogicDynamicBeanProvider logicDynamicBeanProvider;

    @Override
    public void restartThread() {
        budoCanalServer.start();
    }

    /**
     * 为各种入口建立监听
     */
    @Override
    public void onRootApplicationContextRefreshedEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("#54 onRootApplicationContextRefreshedEvent, " + contextRefreshedEvent);

        this.loadDataProducer();
    }

    @Override
    public void loadDataProducer() {
        List<DataNode> sourceDataNodes = dataNodeService.listSourceDataNodes(Page.max());

        for (DataNode sourceDataNode : sourceDataNodes) {
            String url = sourceDataNode.getUrl();
            log.info("#65 loadDataProducer, url=" + url);

            if (url.startsWith("jdbc:mysql://")) {
                logicDynamicBeanProvider.canalDataProducer(sourceDataNode);
                continue;
            }

            if (url.startsWith("async:")) { // 192.168.4.253:9092 tcp://192.168.4.251:61616
                List<Pipeline> pipelines = pipelineService.listBySourceDataNode(sourceDataNode.getId(), Page.max());
                for (Pipeline pipeline : pipelines) {
                    logicDynamicBeanProvider.asyncDataProducer(sourceDataNode, pipeline);
                }
                continue;
            }

            throw new RuntimeException("#80 不支持的数据源头节点, url=" + url + ", sourceDataNode=" + sourceDataNode);
        }

        budoCanalServer.start();
    }
}