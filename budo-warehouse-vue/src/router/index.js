import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import(/* webpackChunkName: "Home" */ '../views/Home.vue')
  },
  {
    path: '/About',
    component: () => import(/* webpackChunkName: "About" */ '../views/About.vue')
  },
  {
    path: '/DataNode',
    component: () => import(/* webpackChunkName: "DataNode" */ '../views/DataNode.vue')
  },
  {
    path: '/Pipeline',
    component: () => import(/* webpackChunkName: "Pipeline" */ '../views/Pipeline.vue')
  },
  {
    path: '/MysqlGrant',
    component: () => import(/* webpackChunkName: "MysqlGrants" */ '../views/MysqlGrant.vue')
  },
  {
    path: '/LogPosition',
    component: () => import(/* webpackChunkName: "LogPosition" */ '../views/LogPosition.vue')
  },
  {
    path: '/EntryBuffer',
    component: () => import(/* webpackChunkName: "EntryBuffer" */ '../views/EntryBuffer.vue')
  },
  {
    path: '/FieldMapping',
    component: () => import(/* webpackChunkName: "FieldMapping" */ '../views/FieldMapping.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
